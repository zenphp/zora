# Release Notes

## [4.1.3](https://gitlab.com/zenphp/zora/compare/v4.1.2...v4.1.3) (2024-03-15)


### Bug Fixes

* remove prepare from package.json ([af52a50](https://gitlab.com/zenphp/zora/commit/af52a5074d448744e283227be59b8b0c1fcfb372))
* updates for release and hooks ([d4a463e](https://gitlab.com/zenphp/zora/commit/d4a463ec7ded68bb4090de5f732fc1e61d0c712c))
